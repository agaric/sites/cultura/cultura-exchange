<?php

/**
 * Create the standard format Filtered HTML.
 */
function cultura_create_filtered_html_format() {
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => '0',
    'filters' => array(
      'filter_url' => array(
        'weight' => '-49',
        'status' => '1',
        'settings' => array(
          'filter_url_length' => '96',
        ),
      ),
      'typogrify' => array(
        'weight' => '-48',
        'status' => '1',
        'settings' => array(
          'smartypants_enabled' => 1,
          'smartypants_hyphens' => '2',
          'wrap_ampersand' => 1,
          'widont_enabled' => 1,
          'hyphenate_shy' => 0,
          'wrap_abbr' => '1',
          'wrap_caps' => 1,
          'wrap_initial_quotes' => 1,
          'wrap_numbers' => '1',
          'ligatures' => array(
            'ffi' => 'ffi',
            'ffl' => 'ffl',
            'ff' => 'ff',
            'fi' => 'fi',
            'fl' => 'fl',
            'ij' => 'ij',
            'IJ' => 'IJ',
            'st' => 0,
            'ss' => 0,
          ),
          'arrows' => array(
            '-&gt;&gt;' => '-&gt;&gt;',
            '&lt;&lt;-' => '&lt;&lt;-',
            '-&gt;|' => '-&gt;|',
            '|&lt;-' => '|&lt;-',
            '&lt;-&gt;' => '&lt;-&gt;',
            '-&gt;' => '-&gt;',
            '&lt;-' => '&lt;-',
            '&lt;=&gt;' => '&lt;=&gt;',
            '=&gt;' => '=&gt;',
            '&lt;=' => '&lt;=',
          ),
          'fractions' => array(
            '1/4' => '1/4',
            '1/2' => '1/2',
            '3/4' => '3/4',
            '1/3' => '1/3',
            '2/3' => '2/3',
            '1/5' => '1/5',
            '2/5' => '2/5',
            '3/5' => '3/5',
            '4/5' => '4/5',
            '1/6' => '1/6',
            '5/6' => '5/6',
            '1/8' => '1/8',
            '3/8' => '3/8',
            '5/8' => '5/8',
            '7/8' => '7/8',
          ),
          'quotes' => array(
            '&lt;&lt;' => '&lt;&lt;',
            '&gt;&gt;' => '&gt;&gt;',
            ',,' => 0,
            '\'\'' => 0,
          ),
          'space_to_nbsp' => 1,
        ),
      ),
      'filter_html' => array(
        'weight' => '-47',
        'status' => '1',
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <u> <cite> <code> <h2> <h3> <h4> <h5> <h6> <ul> <ol> <li> <dl> <dt> <dd> <blockquote> <p> <br> <span> <div> <img> <sub> <sup> <strike>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_autop' => array(
        'weight' => '-46',
        'status' => '1',
      ),
      'filter_htmlcorrector' => array (
        'weight' => '-45',
        'status' => '1',
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);
  return $filtered_html_format;
}


/**
 * Create standard format Full HTML.
 */
function cultura_create_full_html_format() {
  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => '1',
    'filters' => array(
      'filter_url' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(
          'filter_url_length' => '96',
        ),
      ),
      'typogrify' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(
          'smartypants_enabled' => 1,
          'smartypants_hyphens' => '3',
          'wrap_ampersand' => 1,
          'widont_enabled' => 1,
          'hyphenate_shy' => 0,
          'wrap_abbr' => '1',
          'wrap_caps' => 1,
          'wrap_initial_quotes' => 1,
          'wrap_numbers' => '1',
          'ligatures' => array (
            'ffi' => 'ffi',
            'ffl' => 'ffl',
            'ff' => 'ff',
            'fi' => 'fi',
            'fl' => 'fl',
            'ij' => 'ij',
            'IJ' => 'IJ',
            'st' => 0,
            'ss' => 0,
          ),
          'arrows' => array(
            '-&gt;&gt;' => '-&gt;&gt;',
            '&lt;&lt;-' => '&lt;&lt;-',
            '-&gt;|' => '-&gt;|',
            '|&lt;-' => '|&lt;-',
            '&lt;-&gt;' => '&lt;-&gt;',
            '-&gt;' => '-&gt;',
            '&lt;-' => '&lt;-',
            '&lt;=&gt;' => '&lt;=&gt;',
            '=&gt;' => '=&gt;',
            '&lt;=' => '&lt;=',
          ),
          'fractions' => array(
            '1/4' => '1/4',
            '1/2' => '1/2',
            '3/4' => '3/4',
            '1/3' => '1/3',
            '2/3' => '2/3',
            '1/5' => '1/5',
            '2/5' => '2/5',
            '3/5' => '3/5',
            '4/5' => '4/5',
            '1/6' => '1/6',
            '5/6' => '5/6',
            '1/8' => '1/8',
            '3/8' => '3/8',
            '5/8' => '5/8',
            '7/8' => '7/8',
          ),
          'quotes' => array(
            ',,' => 0,
            '\'\'' => 0,
            '&lt;&lt;' => 0,
            '&gt;&gt;' => 0,
          ),
          'space_to_nbsp' => 1,
        ),
      ),
      'filter_autop' => array(
        'weight' => '1',
        'status' => '1',
      ),
      'filter_htmlcorrector' => array(
        'weight' => '10',
        'status' => '1',
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);
  return $full_html_format;
}