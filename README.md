culturaexchange
===============

Code for live Cultura Exchanges (same code for spring or fall).

To reinstall *after* a site has been exported to the Cultura flagship site http://cultura.mit.edu by using the export URL 
(see button on exchange site dashboard), use this command:

`drush site-install cultura --account-mail='agaric-drupal-auto@mit.edu' --site-mail='agaric-drupal-auto@mit.edu'`

Deploying code changes
----------------------

Be sure to install Vagrant: https://www.vagrantup.com/

You will need a virtual machine also; our experience is with https://www.virtualbox.org/

```
vagrant up
vagrant ssh
cd /vagrant/
```

To see available commands:

```
rake -T
```

To deploy to the Spring site:

```
rake deploy_spring
```

To deploy to the Fall site:

```
rake deploy_fall
```

To deploy a third-party school named 'example' would be `rake deploy_example`


Setting up a Cultura Exchange for local testing
-----------------------------------------------

```
vagrant up
vagrant ssh
cd /vagrant/
drush make source/cultura/build-cultura.make web/
```
