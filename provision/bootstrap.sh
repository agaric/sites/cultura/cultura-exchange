#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive
cp -r /vagrant/provision/etc/apt/* /etc/apt/
apt-get update
apt-get -y upgrade
apt-get -y install rake
apt-get -y autoremove

cp -r /vagrant/provision/etc/* /etc/

chmod -R u+w /vagrant/web/sites/default
rm -rf /vagrant/web
cd /vagrant
drush make source/cultura/build-cultura.make web/

chmod -R u+w /vagrant/web/sites/default
rm -rf ~/.drush/cache/*
cp /vagrant/provision/settings.php /vagrant/web/sites/default/

export FILES=/var/local/drupal
if [ ! -d $FILES ]; then
	mkdir -p $FILES
	chown -R www-data:staff $FILES
	chmod -R g+w $FILES
fi

if [ ! -L /vagrant/web/sites/default/files ]; then
	ln -s $FILES /vagrant/web/sites/default/files
fi

if [ ! -d /var/lib/mysql/drupal ]; then
	mysqladmin -u root create drupal
fi

if [ -L /etc/apache2/sites-enabled/000-default ]; then
	a2dissite 000-default
fi

if [ ! -L /etc/apache2/sites-enabled/drupal ]; then
	a2ensite drupal
fi

service apache2 restart

cd /vagrant

drush -y si cultura
