<?php

/**
 * @file
 * Test case for profile
 */

require_once 'DrupalIntegrationTestCase.php';

class CulturaProfileTest extends DrupalIntegrationTestCase {

  public function test_content_types() {
    $this->assertArrayHasKey('page', node_type_get_names());
    $this->assertArrayHasKey(CULTURA_QUESTIONNAIRE_NODE_TYPE, node_type_get_names());
    $this->assertArrayHasKey(CULTURA_DISCUSSION_NODE_TYPE, node_type_get_names());
    $this->assertArrayHasKey(CULTURA_QUESTIONNAIRE_BUILDER_NODE_TYPE, node_type_get_names());
  }

  public function test_text_formats() {
    $this->assertArrayHasKey('plain_text', filter_formats());
    $this->assertArrayHasKey('filtered_html', filter_formats());
    $this->assertArrayHasKey('full_html', filter_formats());
  }

  public function test_block_locations() {
    $GLOBALS['user'] = user_load(1);
    $this->assertArrayHasKey('search_form', block_list('navigation'));
  }

  public function test_vocabularies() {
    $this->assertArrayHasKey(CULTURA_TAGS, taxonomy_vocabulary_get_names());
    $this->assertArrayHasKey(CULTURA_QUESTIONNAIRE_LANGUAGE_VOCABULARY, taxonomy_vocabulary_get_names());
    $this->assertArrayHasKey(CULTURA_QUESTIONNAIRE_VOCABULARY, taxonomy_vocabulary_get_names());
  }

  public function test_roles() {
    $this->assertNotEmpty(user_role_load_by_name('host instructor'));
    $this->assertNotEmpty(user_role_load_by_name('guest instructor'));
    $this->assertNotEmpty(user_role_load_by_name('student'));
    $this->assertNotEmpty(user_role_load_by_name('observer'));
    $this->assertNotEmpty(user_role_load_by_name('administrator'));
  }

  public function test_menu_links() {
    $main_menu = menu_load_links('main-menu');
    $this->assertCount(2, $main_menu);
    $this->assertTrue(cultura_profile_test_case_menu_item_by_path('<front>', $main_menu));
    $this->assertTrue(cultura_profile_test_case_menu_item_by_name('About', $main_menu));
  }

  public function testSampleQuestionnairesAreCreated() {
    $nodes = cultura_questionnaire_unpublished();
    $this->assertCount(3, $nodes);
  }

}


/**
 * Test that a menu item with a given path exists in an array of menu links.
 *
 * If third parameter 'hidden' is provided, further check the link is disabled.
 */
function cultura_profile_test_case_menu_item_by_path($link_path, $menu_links, $hidden = FALSE) {
  foreach ($menu_links as $link) {
    if ($link['link_path'] == $link_path) {
      if ($hidden) {
        return ($link['hidden'] == TRUE);
      }
      return TRUE;
    }
  }
  // If we get here, it didn't find it.
  return FALSE;
}

/**
 * Test that a menu item with a given title exists in an array of menu links.
 *
 * If third parameter 'hidden' is provided, further check the link is disabled.
 */
function cultura_profile_test_case_menu_item_by_name($link_title, $menu_links, $hidden = FALSE) {
  foreach ($menu_links as $link) {
    if ($link['link_title'] == $link_title) {
      if ($hidden) {
        return ($link['hidden'] == TRUE);
      }
      return TRUE;
    }
  }
  // If we get here, it didn't find it.
  return FALSE;
}
