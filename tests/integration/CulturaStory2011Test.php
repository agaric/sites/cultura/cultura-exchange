<?php

require_once 'DrupalIntegrationTestCase.php';

/**
 * @link http://pm.agaric.com/issues/2011
 */
class CulturaStory2011Test extends DrupalIntegrationTestCase {

  /**
   * Test people can see the current year in the site name.
   */
  public function test_site_name_starts_with_current_year() {
    $this->assertStringStartsWith(date('Y'), variable_get('site_name'));
  }

}
