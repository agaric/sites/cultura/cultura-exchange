<?php

/**
 * @file
 * Provides test case for questionnaires
 */

require_once 'DrupalIntegrationTestCase.php';

class CulturaQuestionnaireTest extends DrupalIntegrationTestCase {

  protected $primary_language;

  protected $secondary_language;

  public function setUp() {
    variable_set('cultura_registration_tokens', array(
      'English' => cultura_registration_token('English'),
      'German' => cultura_registration_token('German'),
    ));
    $vocabulary = taxonomy_vocabulary_machine_name_load(CULTURA_QUESTIONNAIRE_LANGUAGE_VOCABULARY);
    $this->primary_language = (object)array('name' => 'English', 'vid' => $vocabulary->vid);
    taxonomy_term_save($this->primary_language);
    $this->secondary_language = (object)array('name' => 'German', 'vid' => $vocabulary->vid);
    taxonomy_term_save($this->secondary_language);

    $this->account = $this->drupalCreateUser();
    $this->account->roles = array(user_role_load_by_name('student')->rid => 'student');
    $this->account->cultura_language[LANGUAGE_NONE][0]['tid'] = $this->primary_language->tid;
    user_save($this->account);
    $this->drupalLogin($this->account);

    foreach (cultura_questionnaire_unpublished() as $node) {
      $node->status = NODE_PUBLISHED;
      node_save($node);
    }

    variable_set('webform_tracking_mode', 'ip_address');
  }

  public function testFindAllPublishedByLanguage() {
    $published = cultura_questionnaire_all_published_by_language($this->primary_language);
    $this->assertCount(3, $published);
  }

  public function testFindNotAnsweredByUser() {
    $unanswered = cultura_questionnaire_questionnaires_not_answered_by_user($this->account);
    $this->assertCount(3, $unanswered);
    $questionnaire = array_shift($unanswered);
    $values = array('op' => t('Submit'));
    foreach ($questionnaire->webform['components'] as $component) {
      $values['submitted'][$component['form_key']] = $this->randomString();
    }
    $this->drupalSubmitForm('webform_client_form', $values, $questionnaire);
    $this->assertEmpty(form_get_errors());
    $this->assertCount(2, cultura_questionnaire_questionnaires_not_answered_by_user($this->account));
  }
}
