<?php

require_once 'DrupalIntegrationTestCase.php';

/**
 * @link http://pm.agaric.com/issues/1947
 */
class CulturaStory1947Test extends DrupalIntegrationTestCase {

  /**
   * Test people cannot see content without logging in.
   */
  public function test_anonymous_users_cannot_see_content() {
    $this->assertEquals(MENU_ACCESS_DENIED, menu_execute_active_handler('node', FALSE));
  }

  /**
   * Test that anonymous users can reach the login and reset password pages.
   */
  public function test_anonymous_users_can_reach_reset_password_page() {
    $this->assertInternalType('array', menu_execute_active_handler('user', FALSE));
    $this->assertInternalType('array', menu_execute_active_handler('user/password', FALSE));
  }

}
