<?php

/*/
 * @file
 * Test case for @link http://pm.agaric.com/issues/1908
 */

require_once 'DrupalIntegrationTestCase.php';

class CulturaStory1908Test extends DrupalIntegrationTestCase {

  public function setUp() {
    $GLOBALS['theme_key'] = variable_get('admin_theme', 'seven');
    $_GET['q'] = 'admin/dashboard';
    variable_set('cultura_registration_tokens', array());
  }

  public function test_registration_tokens_form() {
    $host_instructor = user_role_load_by_name('host instructor');
    $GLOBALS['user'] = $this->drupalCreateUser();
    $GLOBALS['user']->roles[$host_instructor->rid] = $host_instructor->name;

    $blocks = block_list('dashboard_main');

    $this->assertArrayHasKey('cultura_registration_tokens', $blocks);

    $form = $blocks['cultura_registration_tokens']->content;

    $this->assertArrayHasKey('primary', $form);
    $this->assertArrayHasKey('secondary', $form);
    $this->drupalSubmitForm('cultura_registration_tokens_form', array(
      'primary' => 'English',
      'secondary' => 'German',
      'op' => $form['submit']['#value'],
    ));
    $this->assertEmpty(form_get_errors());
    $this->assertArrayHasKey('English', variable_get('cultura_registration_tokens', array()));
    $this->assertArrayHasKey('German', variable_get('cultura_registration_tokens', array()));
  }

  public function test_registration_tokens_validation() {
    $this->drupalSubmitForm('cultura_registration_tokens_form', array(
      'primary' => 'English',
      'secondary' => 'English',
      'op' => t('Generate'),
    ));
    $this->assertNotEmpty(form_get_errors());
    $this->assertArrayHasKey('secondary', form_get_errors());
  }

  public function test_registration_tokens_block_for_secondary_instructor() {
    $educator = user_role_load_by_name('guest instructor');
    $GLOBALS['user'] = $this->drupalCreateUser();
    $GLOBALS['user']->roles[$educator->rid] = $educator->name;
    variable_set('cultura_registration_tokens', array(
      'English' => cultura_registration_token('English'),
      'German' => cultura_registration_token('German'),
    ));

    $blocks = block_list('dashboard_main');

    $this->assertArrayHasKey('cultura_registration_tokens', $blocks);
    $this->assertNotContains(cultura_registration_token('English'), $blocks['cultura_registration_tokens']->content['#markup']);
    $this->assertContains(cultura_registration_token('German'), $blocks['cultura_registration_tokens']->content['#markup']);
  }

}
