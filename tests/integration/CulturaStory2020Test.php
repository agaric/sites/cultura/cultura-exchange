<?php

/**
 * @file
 * Test case for @link http://pm.agaric.com/issues/2020
 */

require_once 'DrupalIntegrationTestCase.php';

class CulturaStory2020Test extends DrupalIntegrationTestCase {

  public function setUp() {
    variable_set('cultura_registration_tokens', array(
      'English' => cultura_registration_token('English'),
      'German' => cultura_registration_token('German'),
    ));
  }

  /**
   * Test people cannot register without an invitation.
   */
  public function test_registration_requires_token() {
    $valid_token = cultura_registration_token('English');
    $invalid_token = drupal_hmac_base64('English', 'salt');
    $this->assertEquals(MENU_ACCESS_DENIED, menu_execute_active_handler('user/register', FALSE));
    $this->assertEquals(MENU_ACCESS_DENIED, menu_execute_active_handler("user/register/$invalid_token", FALSE));
    $this->assertInternalType('array', menu_execute_active_handler("user/register/$valid_token", FALSE));
  }

  /**
   * Test registration links are not displayed.
   */
  public function test_registration_link_is_not_displayed() {
    $this->assertArrayHasKey('links', drupal_get_form('user_login_block'));
    $this->assertNotContains('user/register', drupal_get_form('user_login_block')['links']['#markup']);
  }

  /**
   * Test anonymous visitors cannot post content.
   */
  public function test_anonymous_users_cannot_post_content() {
    foreach (node_type_get_types() as $type) {
      $this->assertEquals(MENU_ACCESS_DENIED, menu_execute_active_handler('node/add/' . strtr($type->type, '_', '-'), FALSE));
    }
    $this->assertFalse(user_access('post comment'), drupal_anonymous_user());
  }

}
