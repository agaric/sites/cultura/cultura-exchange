<?php

/**
 * @file
 * Test case for user registration
 */

require_once 'DrupalIntegrationTestCase.php';

class CulturaRegistrationTest extends DrupalIntegrationTestCase {

  /**
   * Sets language tokens.
   */
  public function setUp() {
    variable_set('cultura_registration_tokens', array(
      'English' => cultura_registration_token('English'),
      'German' => cultura_registration_token('German'),
    ));
    $vocabulary = taxonomy_vocabulary_machine_name_load(CULTURA_QUESTIONNAIRE_LANGUAGE_VOCABULARY);
    $term = (object)array('name' => 'English', 'vid' => $vocabulary->vid);
    taxonomy_term_save($term);
    $term = (object)array('name' => 'German', 'vid' => $vocabulary->vid);
    taxonomy_term_save($term);
  }

  /**
   * Tests a user can register with a language obtained from a token.
   */
  public function testRegisterWithLanguage() {
    $token = variable_get('cultura_registration_tokens')['English'];
    $_GET['q'] = "user/register/$token";
    $this->drupalSubmitForm('user_register_form', array(
      'name' => 'robo-user',
      'mail' => 'robouser@example.com',
      'pass' => array(
        'pass1' => 'test',
        'pass2' => 'test',
      ),
      'full_name' => array(LANGUAGE_NONE => array(0 => array('value' => 'Robo User'))),
    ));
    $this->assertEmpty(form_get_errors());
    $this->assertEquals('English', taxonomy_term_load(user_load_by_name('robo-user')->cultura_language[LANGUAGE_NONE][0]['tid'])->name);
  }

}
