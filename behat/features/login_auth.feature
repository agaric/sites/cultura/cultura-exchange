# features/login_auth.feature
@api
Feature: Login authentication
    In order to access the administrative interface
    As a visitor
    I need to be able to log in to the website

    Background:
        Given there are following users:
            | username | email       | plain_password | enabled |
            | Publisher      | micky@metts.com | candy            | yes     |

    Scenario: Log in with username and password
        Given I am on "/user"
         When I fill in the following:
            | username | Publisher |
            | password | candy |
          And I press "Login"
         Then I should be on "/user/"
          And I should see "Logout"

    Scenario: Log in with bad credentials
        Given I am on "/user"
         When I fill in the following:
            | username | bar@foo.com |
            | password | bar         |
          And I press "Login"
         Then I should be on "/admin/login"
          And I should see "Invalid username or password"
