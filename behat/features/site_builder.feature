Feature: Site building
  In order to add features to my Drupal site
  As a site builder
  I need to be able to administer content types

  @drush
  Scenario: Reach the manage fields page of the article content type
    Given I am logged in as a user with the "developer" role
    When I am at "admin/structure/types"
    And I click "manage fields" in the "Page" row
    Then I should be on "admin/structure/types/manage/page/fields"
    And I should see text matching "Add new field"
