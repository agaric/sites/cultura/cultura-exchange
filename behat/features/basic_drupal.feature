@api
Feature: Basic Drupal capabilities
  In order to prove Drupal and DrupalContext are working properly
  As a developer
  I need to use the step definitions of this context

  @drush
  Scenario: Create and log in as a user
    Given I am logged in as a user with the "authenticated user" role
    When I click "My account"
    Then I should see the heading "History"

  @drush
  Scenario: Clear cache
    Given the cache has been cleared
    And I am logged in as a user with the "authenticated user" role
    When I am on the homepage
    Then I should get a "200" HTTP response

  @drush
  Scenario: Visitor is denied access
    Given I am an anonymous user
    When I am on the homepage
    Then I should get a "403" HTTP response
